<?php

return [

    /**
     * Plugin name
     *
     */
    'name' => 'People',

    /**
     * Plugin services
     *
     */
    'services' => [
        'person' => Plugin\People\Services\PersonService::class
    ]
];
