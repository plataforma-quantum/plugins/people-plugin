<?php

namespace Plugins\People\Entities;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    /**
     * Table name on database
     *
     */
    protected $table = 'people_people';

    /**
     * Guarded fields
     *
     */
    protected $guarded = [];
}
