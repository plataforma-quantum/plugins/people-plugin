<?php

namespace Plugins\People\Services;

use Plugins\People\Entities\Person;
use Quantum\Models\Service;

class PersonService extends Service
{

    /**
     * Service Model Instance
     *
     */
    protected $model = Person::class;
}
